set Alimenti;

set Principi;

param min_P{Principi}>=0;

param costo{Alimenti}>=0;

param quantita{Principi,Alimenti}>=0;

param porzioni{Alimenti};

var x{j in Alimenti}>=0, <=porzioni[j];

minimize costo_totale: sum{j in Alimenti}costo[j]*x[j];
subject to vincoli{i in Principi}: sum{j in Alimenti} quantita[i,j]*x[j]>= min_P[i];

data;

set Alimenti:= A1 A2 A3 A4 A5;

set Principi:=  calorie proteine calcio;

param min_P:=
calorie 2000
proteine 50
calcio 700;

param costo:= A1 2
              A2  3
              A3 4
              A4 19
              A5 20;
                 
param quantita: A1    A2  A3 A4 A5:=

calorie         110 160 180 260 420 

proteine         4 8 13 14 4

calcio         2 285 54 80 22;

param porzioni:= A1 4
              A2  8
              A3 3
              A4 2
              A5 2;


solve;

display _varname,_var;
 