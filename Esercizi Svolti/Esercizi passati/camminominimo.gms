$ontext

Esempio di ricerca dell'albero dei cammini minimi dal nodo A verso tutti gli altri nodi
usando la matrice di incidenza nodi-archi (-1 per il nodo di partenza e 1 per il nodo di arrivo).

Se si vuole il cammino minimo da A verso il nodo, ad esempio, E porre  d(i) domande /A -1,B 0,C 0,D 0,E 1/.
$Offtext


sets
   i 'nodes' /A,B,C,D,E/
   j 'arcs' /AB, AC, BD, BE, CB, CD, DE/
;


parameters
c(j) costi archi /AB 3, AC 4,BD 2,BE 5,CB 1,CD 3,DE 6/
d(i) domande /A -4,B 1,C 1,D 1,E 1/
*cap(j) capacit� /AB 6, AC 4, BC 2, BE 7,CD  8,DB 4,DE 5/  ;

table m(i,j)

  AB AC BD BE CB CD DE
A -1 -1  0  0  0  0  0
B  1  0 -1 -1  1  0  0
C  0  1  0  0 -1 -1  0
D  0  0  1  0  0 1  -1
E  0  0  0  1  0  0  1   ;


variables
x(j),
costo 'costo totale';

*x.up(j)=cap(j);
integer variable x;



equations
costotot
bildom(i) bilancio domanda;

costotot..sum(j, c(j)*x(j))=e=costo;
bildom(i)..sum(j,m(i,j)*x(j))=e=d(i);

model path /all/;
 solve path minimizing costo using mip;

$Ontext

sets
   i 'nodes' /A,B,C,D,E/;

alias(i,j);
set arc(i,j) /A.B, A.C, B.D, B.E, C.B, C.D, D.E/;

parameters
c(i,j) costi archi /A.B 3, A.C 4,B.D 2,B.E 5,C.B 1,C.D 3,D.E 6/
d(i) domande /A -1,B 0,C 0,D 0,E 1/

variables
x(i,j),
costo 'costo totale';
positive variable x;



equations
costotot ,
bildom(i) bilancio domanda;

costotot..sum((i,j)$arc(i,j), c(i,j)*x(i,j))=e=costo;
bildom(i)..sum(j, x(j,i)-x(i,j))=e=d(i);

model path /all/;
 solve path minimizing costo using mip;
$Offtext
