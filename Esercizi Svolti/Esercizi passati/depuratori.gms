

set I citt� /C1, C2,C3,C4,C5/;
set J aree /A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12/;

Parameters

costi(J)
/A1 7,A2 9,A3 12,A4 3,A5 4,A6 4,A7 5,A8 11,A9 8 ,A10 6,A11 7,A12 16/

capacity(J)
/A1 15,A2 39,A3 26,A4 31,A5 34,A6 24,A7 51,A8 19,A9 18 ,A10 36,A11 41,A12 34/;

Table a(I,J)    distanza

  A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12
C1 1  0  1  0  1  0  1  1  0  0   0   1
C2 0  1  1  0  0  1  0  0  1  0   1   1
C3 1  1  0  0  0  1  1  0  0  1   0   0
C4 0  1  0  1  0  0  1  1  0  1   0   1
C5 0  0  0  1  1  1  0  0  1  1   1   1;



Variables
x(J)  impianti,
z     costo totale  ;

Binary Variable x;


Equations
constr1(I), constr2(I)
costo;

constr1(I).. sum(J,a(I,J)*x(J))=g=1;
constr2(I).. sum(J,a(I,J)*x(J))=e=1;
costo..z=e= sum(J,costi(J)*x(J));

Model covering /constr1, costo /;
Solve covering minimizing z using mip;

Model partitioning / constr2, costo/;

Solve partitioning minimizing z using mip;