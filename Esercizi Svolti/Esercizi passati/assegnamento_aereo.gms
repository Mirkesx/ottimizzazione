Set
I /A1*A5/,
J /P1*P5/;

Table a(I,J)

      P1  P2   P3  P4  P5
A1    15  20    8  16  12
A2    17   9   15  25  12
A3    12  32   16   9  20
A4     0  15    9   7  30
A5     0   0   35   10 18 ;



Variables
x(I,J),
z;

Binary variable x;

Equations
vincolo1(I),
vincolo2(J),
obj;


vincolo1(I).. sum(J,x(I,J))=e=1;
vincolo2(J).. sum(I,x(I,J))=e=1;
obj..z=e= sum((I,J),a(I,J)*x(I,J));

Model assegn / all /;

Solve assegn maximizing z using mip;