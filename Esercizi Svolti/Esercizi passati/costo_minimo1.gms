sets
   i 'nodes' /A,B,C,D,E/
   j 'arcs' /AB, AC, BC, BE, CD, DB, DE/
;


parameters
c(j) costi archi /AB 2, AC 3,BC 1,BE 5,CD 1,DB 2,DE 4/
d(i) domande /A -5,B 6,C -5,D 0,E 4/
cap(j) capacit� /AB 6, AC 4, BC 2, BE 7,CD  8,DB 4,DE 5/  ;

table m(i,j)  matrice incidenza

  AB AC BC BE CD DB DE
A -1 -1  0  0  0  0  0
B  1  0 -1 -1  0  1  0
C  0  1  1  0 -1  0  0
D  0  0  0  0  1 -1 -1
E  0  0  0  1  0  0  1   ;


variables
x(j),
costo 'costo totale';

positive variable x;
x.up(j)=cap(j);

equations
costotot
bildom(i) bilancio domanda;

costotot..sum(j, c(j)*x(j))=e=costo;
bildom(i)..sum(j,m(i,j)*x(j))=e=d(i);

model flusso /all/;
 solve flusso minimizing costo using lp;
