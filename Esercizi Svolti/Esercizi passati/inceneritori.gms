

set I citt� /C1*C7/;
set J aree /A1*A10/;

Parameters

costi(J)
/A1 4,A2 7,A3 8,A4 4,A5 6,A6 9,A7 10,A8 10,A9 8 ,A10 6/,

capacity(J)
/A1 450,A2 720,A3 580,A4 460,A5 660,A6 390,A7 510,A8 1000,A9 830 ,A10 680/;

Table a(I,J)  vicinanza

  A1 A2 A3 A4 A5 A6 A7 A8 A9 A10
C1 0  1  0  0  1  1  1  0  1  0
C2 1  1  0  1  0  0  0  1  0  0
C3 0  0  0  0  1  0  0  1  1  0
C4 1  0  1  0  1  0  1  1  1  1
C5 0  0  1  1  0  0  1  0  0  1
C6 0  1  0  0  0  0  0  1  1  0
C7 0  0  0  1  0  1  1  1  0  1  ;


Variables
x(J)  impianti,
z     capacit� totale  ;

Binary Variable x;


Equations
constr1(I), constr2(I)
costo;

constr1(I).. sum(J,a(I,J)*x(J))=l=1;
constr2(I).. sum(J,a(I,J)*x(J))=e=1;
costo..z=e= sum(J,costi(J)*x(J));

Model packing /constr1, costo /;
Solve packing maximizing z using mip;

Model partitioning / constr2, costo/;

Solve partitioning maximizing z using mip;
